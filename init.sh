#!/bin/bash
BASE_PATH=$(cd `dirname $0`;pwd)
source $BASE_PATH/libexec/color.sh
read -p "请输入Gitee绑定的主邮箱: " email
echo $email > $Gitee_Repo/etc/email
read -p "请输入Gitee密码: " passwd
echo $passwd > $Gitee_Repo/etc/passwd
read -p "请输入Gitee工具集应用的client_id: " client_id
echo $client_id > $Gitee_Repo/etc/client_id
read -p "请输入Gitee工具集应用的client_secret: " client_secret
echo $client_secret > $Gitee_Repo/etc/client_secret
echo -e "创建临时文件目录，您可能需要输入密码\n"
mkdir /tmp/GiteeTools
sudo chown -R $USER:staff /tmp/GiteeTools
sudo chmod 777 /tmp/GiteeTools
gitee nat
echo -e "${GREEN}初始化完成~${NC}"
clear


