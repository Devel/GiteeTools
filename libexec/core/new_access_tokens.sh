#!/bin/bash

export main_email=$(cat $Gitee_Repo/etc/email)
export passwd=$(cat $Gitee_Repo/etc/passwd)
export client_id=$(cat $Gitee_Repo/etc/client_id)
export client_secret=$(cat $Gitee_Repo/etc/client_secret)
curl -X POST --data-urlencode "grant_type=password" --data-urlencode "username=${main_email}" --data-urlencode "password=${passwd}" --data-urlencode "client_id=${client_id}" --data-urlencode "client_secret=${client_secret}" --data-urlencode "scope=user_info projects pull_requests issues notes keys hook groups gists enterprises" https://gitee.com/oauth/token
printf "\n"
read -p "请输入access_token:" access_token
echo $access_token > $Gitee_Repo/etc/access_token
clear
