#!/bin/bash
BASE_PATH=$(cd `dirname $0`;pwd)
export BLACK="\033[0;30m"  
export DARK_GRAY="\033[1;30m"  
export BLUE="\033[0;34m"  
export LIGHT_BLUE="\033[1;34m"  
export GREEN="\033[0;32m"  
export LIGHT_GREEN="\033[1;32m"  
export CYAN="\033[0;36m"  
export LIGHT_CYAN="\033[1;36m"  
export RED="\033[0;31m"  
export LIGHT_RED="\033[1;31m"  
export PURPLE="\033[0;35m"  
export LIGHT_PURPLE="\033[1;35m"  
export BROWN="\033[0;33m"  
export YELLOW="\033[0;33m"  
export LIGHT_GRAY="\033[0;37m"  
export WHITE="\033[1;37m" 
export NC="\033[0m"
echo -e "${CYAN}欢迎来到Gitee工具集安装程序，${NC}\c"
echo -e "请问您是否在Gitee工具集的安装目录？(我们将要把环境变量指向本目录)"
echo "[1] 是的"
echo "[2] 不"
echo "[3] 不论是不是，我都想把Gitee工具集安装到指定目录(直接写入bin,libexec等)"
echo "[4] 不论是不是，我的想把Gitee工具集安装的/usr/local"
echo "[5] 不论是不是，我的想把Gitee工具集安装的/opt/local"
if ! read -t 10 -p "输入选项(十秒钟之内选择，默认[5]):" xx
then
    echo -e "${RED}抱歉，10秒已到${NC}"
    sleep 3
    clear
fi
if (($xx == 1));then
{
    export installrepo="${pwd}"
}
fi
if (($xx == 2 || $xx == 3));then
{
    read -p "请输入安装地址: " installrepo
}
fi
if (($xx == 4));then
{
    export installrepo=/usr/local/GiteeTools
}
fi
if (($xx == 5));then
{
    export installrepo=/opt/local/GiteeTools
}
fi
if (( ! $xx == 1 || $xx == 2 || $xx == 3 || $xx == 4 || $xx == 5 ));then
{
    echo -e "${RED}Error: ${NC}\c"
    echo "请输入正确的选项！"
}
fi
if ((! $xx == 1));then
{
    cp -R $BASE_PATH/.. $installrepo
}
fi
echo -e "${YELLOW}安装完成，3秒后开始初始化~${NC}"
sleep 3
printf "\033c"
$BASE_PATH/../init.sh

